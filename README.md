# Probabilistic Principal Components and Covariates Analysis

JAX implementation of the Probabilistic Principal Components and Covariates Analysis algorithm from Probabilistic principal component analysis for
metabolomic data, Nyamundanda et al., BMC Bioinformatics, 2010 [link](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/1471-2105-11-571)

Run `python digit_example.py` to compare PCA and PPCA on scikit-learn digit dataset with label as covariables. The output should look like:
![digit_dataset_ex](illus.png)

# Documentation

[https://hgangloff.pages.mia.inra.fr/xpcax/index.html](https://hgangloff.pages.mia.inra.fr/xpcax/index.html)

# Install/upgrade

```
pip install --upgrade xpcax --extra-index-url https://forgemia.inra.fr/api/v4/projects/8098/packages/pypi/simple
```

# Contributing

Install pre-commit

```
pip install pre-commit
```

Initialize pre-commit in the repo

```
pre-commit install
```
