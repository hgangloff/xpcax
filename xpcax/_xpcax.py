from functools import partial
import jax
import jax.numpy as jnp
import numpy as np

import statsmodels.api as sm


@partial(jax.jit, static_argnums=1)
def ppca(X, q):
    """
    EM algorithm for Probabilistic PCA

    Parameters
    ----------
    X : array
      The array of data on which to perform PPCA of shape (n_samples,
      n_features)
    q : integer
      The dimension of the feature space to learn

    Returns
    -------
    double
      the value of the average likelihood under the learnt model
    double
      the value of the noise variance in the learnt model
    jnp array
      the score, ie the project of the samples in feature space that has been
      learnt
    """

    max_iter = 1000
    N, p = X.shape  # number of observations and variables

    muhat = jnp.mean(X, axis=0, keepdims=1)
    Xc = X - muhat

    # init the model
    S = 1 / N * (Xc.T @ Xc)
    temp_val, temp_vec = jnp.linalg.eig(S)
    idx_sorting = jnp.argsort(temp_val)[::-1]  # because it is sort in decreasing
    temp_val = jnp.real(temp_val[idx_sorting])
    temp_vec = jnp.real(temp_vec[:, idx_sorting])
    sig2 = jnp.abs(1 / (p - q)) * jnp.sum(temp_val[q : p + 1])  # start val for
    # variance, it is a scalar
    W = temp_vec[:, :q]  # start val for loading, proj matrix of dim (p, q)

    def _scan_fun(carry, _):
        W, sig2 = carry

        # E-Step
        M_1 = jnp.linalg.inv(W.T @ W + sig2 * jnp.diag(jnp.ones(q)))

        _x_ = M_1 @ (W.T @ Xc.T)  # dim (q, N) or (q, 1) if we strictly follow
        # the formula but we have vectorized the computation over all the samples
        # thanks to the matrix product

        sum_xx_ = N * sig2 * M_1 + (_x_ @ _x_.T)  # dim (q, q), same remark, it has
        # been vectorized. Note the N * because it is **sum**_xx_

        # M-Step
        W = (Xc.T @ _x_.T) @ jnp.linalg.inv(sum_xx_)  # dim (p, q), estimation
        # of the loadings, same remark, it has been vectorized

        sig2 = (
            1
            / (N * p)
            * (
                jnp.sum(jnp.square(Xc), axis=(0, 1))
                - 2 * jnp.trace(_x_.T @ W.T @ Xc.T)
                + jnp.trace((W.T @ W) @ sum_xx_)
            )
        )

        return (W, sig2), _x_

    (W, sig2), list_x_ = jax.lax.scan(_scan_fun, (W, sig2), jnp.arange(max_iter))

    # compute loglikelihood
    C = sig2 * jnp.diag(jnp.ones(p)) + W @ W.T
    avg_llkh = (
        -1
        / 2
        * (
            p * jnp.log(2 * jnp.pi)
            + jnp.log(jnp.linalg.det(C))
            + jnp.trace(jnp.linalg.inv(C) @ S)
        )
    )

    return avg_llkh, sig2, list_x_[-1].T


def ppcca(X, covars, q):
    """
    EM algorithm for Probabilistic Principal Components and Covariates Analysis

    Parameters
    ----------
    X : array
      The array of data on which to perform PPCCA of shape (n_samples,
      n_features)
    covars : array
      The array of covariables to consider of shape (n_samples, n_covariables)
    q : integer
      The dimension of the feature space to learn

    Returns
    -------
    double
      the value of the average likelihood under the learnt model
    double
      the value of the noise variance in the learnt model
    jnp array
      the score, ie the project of the samples in feature space that has been
      learnt
    """

    max_iter = 1000
    N, p = X.shape  # number of observations and variables

    null_alpha = False
    if covars is None:
        L = 1
        covars = jnp.ones((L + 1, N))
        null_alpha = True
    else:
        L = covars.shape[1]
        # standardize the covars for stability
        covars = (covars - jnp.amin(covars, axis=0, keepdims=1)) / (
            jnp.amax(covars, axis=0, keepdims=1) - jnp.amin(covars, axis=0, keepdims=1)
        )
        covars = jnp.concatenate(
            [jnp.ones((1, N)), covars.T], axis=0
        )  # now dim (L + 1, N)

    muhat = jnp.mean(X, axis=0, keepdims=1)
    Xc = X - muhat

    # init the model
    S = 1 / N * (Xc.T @ Xc)
    temp_val, temp_vec = jnp.linalg.eig(S)
    idx_sorting = jnp.argsort(temp_val)[::-1]  # because it is sort in decreasing
    temp_val = jnp.real(temp_val[idx_sorting])
    temp_vec = jnp.real(temp_vec[:, idx_sorting])
    sig2 = jnp.abs(1 / (p - q)) * jnp.sum(
        temp_val[q : p + 1]
    )  # init variance, it is a scalar
    W = temp_vec[:, :q]  # init proj matrix of dim (p, q)

    # initialization of alpha
    if null_alpha:
        alpha = jnp.zeros((q, L + 1))
    else:
        scores = jnp.transpose(
            jnp.linalg.inv((W.T @ W) + (sig2 * jnp.diag(jnp.ones(q)))) @ W.T @ Xc.T
        )
        alpha = np.empty((q, L + 1))
        for i in range(q):
            alpha[i] = (
                sm.GLM(
                    np.asarray(scores[:, i]),
                    np.asarray(covars[0:,]).T,
                    family=sm.families.Gaussian(),
                )
                .fit()
                .params
            )
        alpha = jnp.asarray(alpha)

    def _scan_fun(carry, _):
        W, sig2, alpha = carry

        # E-Step
        M_1 = jnp.linalg.inv(W.T @ W + sig2 * jnp.diag(jnp.ones(q)))

        _x_ = M_1 @ (
            W.T @ Xc.T + sig2 * (alpha @ covars)
        )  # dim (q, N) or (q, 1) if we strictly follow
        # the formula but we have vectorized the computation over all the samples
        # thanks to the matrix product

        sum_xx_ = N * sig2 * M_1 + (_x_ @ _x_.T)  # dim (q, q), same remark, it has
        # been vectorized. Note the N * because it is **sum**_xx_

        # M-Step
        alpha = (_x_ @ covars.T) @ jnp.linalg.inv(covars @ covars.T)  # estimation
        # of the regression coefficient, vectorized
        W = (Xc.T @ _x_.T) @ jnp.linalg.inv(sum_xx_)  # dim (p, q), estimation
        # of the loadings, same remark, it has been vectorized

        sig2 = (
            1
            / (N * p)
            * (
                jnp.sum(jnp.square(Xc), axis=(0, 1))
                - 2 * jnp.trace(_x_.T @ W.T @ Xc.T)
                + jnp.trace((W.T @ W) @ sum_xx_)
            )
        )

        return (W, sig2, alpha), _x_

    (W, sig2, alpha), list_x_ = jax.lax.scan(
        _scan_fun, (W, sig2, alpha), jnp.arange(max_iter)
    )

    # compute loglikelihood
    x_centered = list_x_[-1] - (alpha @ covars)
    C = sig2 * jnp.diag(jnp.ones(p)) + W @ W.T
    avg_llkh = (
        -1
        / 2
        * (
            p * jnp.log(2 * jnp.pi)
            + jnp.log(jnp.linalg.det(C))
            + jnp.trace(jnp.linalg.inv(C) @ S)
            + q * jnp.log(2 * jnp.pi)
            + jnp.trace(1 / N * (x_centered.T @ x_centered))
        )
    )

    return avg_llkh, sig2, list_x_[-1].T
