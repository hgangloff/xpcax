.. xpcax documentation master file, created by
   sphinx-quickstart on Thu Apr 27 16:28:09 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to xpcax's documentation!
=================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   ./module.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
