#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets

from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder

# from sklearn.decomposition import PCA

from xpcax import ppca
from xpcax import ppcca

random_state = 0

# Load digits dataset
x, y = datasets.load_digits(return_X_y=True)

scaler = StandardScaler()
x_scaled = scaler.fit_transform(x)

one_hot_encoder = OneHotEncoder(sparse_output=False, drop="first")

q = 2

# pca = PCA(n_components=q, random_state=random_state)
# pca.fit(x_scaled)
# x_embedded_pca = pca.transform(x_scaled)
# print("Scikit-learn Probabilistic PCA")
# print(
#    f"Estimated noise variance {pca.noise_variance_:0.2f}"
#    f" , estimated average loglikelihood {pca.score(x_scaled):0.2f}"
# )

avg_llkh_ppca, sig2_ppca, x_embedded_ppca = ppca(x_scaled, q=q)
print("Probabilistic PCA")
print(
    f"Estimated noise variance {sig2_ppca:0.2f}"
    f", estimated average loglikelihood {avg_llkh_ppca:0.2f}"
)

covars = one_hot_encoder.fit_transform(y.reshape((-1, 1)))

avg_llkh_ppcca, sig2_ppcca, x_embedded_ppcca = ppcca(x_scaled, covars=covars, q=q)
print("Probabilistic PCCA")
print(
    f"Estimated noise variance {sig2_ppcca:0.2f}"
    f", estimated average loglikelihood {avg_llkh_ppcca:0.2f}"
)

fig, axes = plt.subplots(1, 2)
axes[0].scatter(x_embedded_ppca[:, 0], x_embedded_ppca[:, 1], c=y)
axes[0].set_xlabel("PC1")
axes[0].set_ylabel("PC2")
axes[0].set_title("Probabilistic PCA")
scatter = axes[1].scatter(
    x_embedded_ppcca[:, 0], x_embedded_ppcca[:, 1], c=y
)  # , cmap="Set1")
axes[1].set_xlabel("PC1")
axes[1].set_ylabel("PC2")
axes[1].set_title("Probabilistic PCCA")
fig.show()
plt.legend(handles=scatter.legend_elements()[0], labels=list(np.arange(10)))
plt.show()
